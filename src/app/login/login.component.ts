import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  returnUrl: any;

  constructor(private formBuilder: FormBuilder) { }
  get request(){
    return this.loginForm.controls
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username:['', [Validators.required]] ,
      password: ['', [Validators.required]],
  })
}
Submit(){
}

getusernnameError(){
  return this.request.username.hasError('required')?'This field is required':'';
}
getpasswordError(){
  return this.request.password.hasError('required')?'This field is required':'';
}

}
