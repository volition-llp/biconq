import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  date = new Date('2019-01-26T00:00:00');
  
  triggerFunction() {
    console.log('Timer Ended');
  }

  constructor() { }

  ngOnInit() {
  }

}
