import { Injectable } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } 		from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

import { environment } from '../../environments/environment';
let httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class BaseserviceService {
  private previousURL: 	string;
	private currentURL: 	string;
  constructor(private route: ActivatedRoute,
		private router: Router,
    private http: HttpClient,
    private snackBar: MatSnackBar) { 
      this.currentURL = this.router.url;
		router.events.subscribe(event => {
			if(event instanceof NavigationEnd){
				this.previousURL 	= this.currentURL;
				this.currentURL 	= event.url;
			}
		});
    }
    public getCurrentURL(){
      return this.currentURL;
    }
    public logoutUser(){
      let key = btoa('currentUser');
      localStorage.removeItem(key);
      return true;
    }
  
    public getPreviousURL(){
      return this.previousURL;
    }
    public snack(message: string){
      let snacked = this.snackBar.open(message, 'OK', {
        duration: environment.SNACK_TIMEOUT
      });
    }
  
    public snackUndo(message: string){
      let snakced = this.snackBar.open(message, 'Undo', {
        duration: environment.SNACK_TIMEOUT
      });
  
      snakced.afterDismissed()
      .subscribe(() => {
        //callback
      });
    }
    public handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {
        console.log('Error Handler');
        // TODO: send the error to remote logging infrastructure
        console.error(error); // log to console instead
          if(error.status == environment.RES_FORBIDDEN){
            this.logoutUser();
            // let returnURL = this.route.snapshot.queryParams['returnUrl'] || '';
            let returnURL = this.router.url || 'login';
            this.router.navigate(['login'], {queryParams: {returnUrl: returnURL}});
          }else{
  
            if(error.error.message && error.error.message != undefined){
              this.snack(error.error.message);
            }else {
              this.snack(environment.SYSTEM_ERROR);
            }
          }
  
        // TODO: better job of transforming error for user consumption
        console.log(`${operation} failed: ${error.message}`);
  
        // Let the app keep running by returning an empty result.
        return of(result as T);
      };
    }
  
}
