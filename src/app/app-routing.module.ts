import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { QuestionComponent } from './question/question.component';
import { ExplanationComponent } from './explanation/explanation.component';
import { Explanation1Component } from './explanation1/explanation1.component';
import { Explanation2Component } from './explanation2/explanation2.component';
import {EmployeeCreateComponent} from './employee-create/employee-create.component';
import {EmployeeListComponent} from './employee-list/employee-list.component';
import {EmployeeEditComponent} from './employee-edit/employee-edit.component';
import { GameviwerComponent } from './gameviwer/gameviwer.component';

import { ContactComponent } from './contact/contact.component';
import { ServiceComponent } from './service/service.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { VerificationComponent } from './verification/verification.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'create-employee' },
  {path:'dashboard',component:DashboardComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'forms',component:QuestionComponent},
  {path:'100',component:ExplanationComponent},
  {path:'200',component:Explanation1Component},
  {path:'300',component:Explanation2Component},
  { path: 'create-employee', component: EmployeeCreateComponent },
  { path: 'employees-list', component: EmployeeListComponent },
  { path: 'employee-edit/:id', component: EmployeeEditComponent },
  {path:'game',component:GameviwerComponent},
  
  {path:'contact',component:ContactComponent},
  {path:'service',component:ServiceComponent},
  {path:'sign',component:SignUpComponent},
  {path:'verification',component:VerificationComponent}

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
