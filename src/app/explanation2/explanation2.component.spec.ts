import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Explanation2Component } from './explanation2.component';

describe('Explanation2Component', () => {
  let component: Explanation2Component;
  let fixture: ComponentFixture<Explanation2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Explanation2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Explanation2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
