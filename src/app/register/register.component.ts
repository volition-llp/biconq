import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from '../common.service';
import { Router,ActivatedRoute} from '@angular/router';
import * as CryptoJS from 'crypto-js';  

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  // pattern
  text:string;
  status:string;
  validString: '[a-z A-Z\s]*';
  validEmail: '';
  validMobile: '[4-9]{1}[0-9]{9}'
  validTelephone: '[0-9]*';
  validZipcode: '[1-9][0-9]{4,5}';
  
  currentmode = 'signup';
  registration: FormGroup;
  name:any;
  email:any;
  phone:any;
  company_name:any;
  occupation:any;
  recent_education:any;
  subscription_code:any;
  item:any;

  constructor(  private formBuilder: FormBuilder,
    private router: Router,
    private  activated:ActivatedRoute,
    private commonservce:CommonService) { }

  get request(){
    return this.registration.controls
  }
  
 ngOnInit() {
    this.registration = this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern(this.validString)]],
      email: ['', [Validators.required,Validators.email]],
      phone: ['', [Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      company_name: ['', [Validators.required]],
      occupation: ['', [Validators.required]],
      recent_education: ['', [Validators.required]],
      subscription_code: ['', [Validators.required]],
  })
  this.Submit();

}
  Submit(){
    
    console.log("this.registration.value",this.registration.value)
    if(this.registration.invalid){ return;} 
    

    this.commonservce.registration(this.registration.value)

    .subscribe(data=>{ 
      this.commonservce.loadDrugs(data)
      console.log(data);
      
      if(data.status=='900')
      {
        console.log("Registration successfully");
         //token generation
     var string=function() {
        
          var text = "";
          var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        
          for (var i = 0; i < 32; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        
          return text; 
        }
let data=string();
  
  console.log(data);
      
      this.router.navigate(['verification'],{
          queryParams:{string:string()},
          
        });
       
    }
      else
      {
        console.log("Registration  error");
      }
    }) 
  }
  getnameError(){
    return this.request.name.hasError('required')?'This field is required':
    this.request.name.hasError('pattern')?'You have entered invalid Name':'';
  }
  getemailError(){
    return this.request.email.hasError('required')?'This field is required':
    this.request.email.hasError('pattern')?'You have entered invalid Email':'';
  }
  getphoneError(){
    return this.request.phone.hasError('required')?'This field is required':
    this.request.phone.hasError('pattern')?'You have entered invalid phone number':'';
  }

  getcompanynameError(){
    return this.request.company_name.hasError('required')?'This field is required':'';
  }
  getoccupationError(){
    return this.request.occupation.hasError('required')?'This field is required':'';
  }
  getrecenteducation(){
    return this.request.recent_education.hasError('required')?'This field is required':'';
  }
  getsubscriptioncode(){
    return this.request.subscription_code.hasError('required')?'This field is required':'';
  }
}
