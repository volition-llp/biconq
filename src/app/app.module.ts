import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule}  from'@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule,HTTP_INTERCEPTORS} from '@angular/common/http';
import{ MatToolbarModule,MatCardModule, MatButton, MatSnackBarModule,MatButtonModule, MatMenuModule, MatFormFieldModule, MatInputModule, MatSlideToggle, MatSlideToggleModule, MatOptionModule, MatRadioModule, MatSelect, MatIconModule, MatSnackBar} from'@angular/material';
import { FormsModule } from '@angular/forms';
import 'hammerjs';

import {ReactiveFormsModule} from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { QuestionComponent } from './question/question.component';
import { CountdownTimerModule } from 'angular-countdown-timer';
import { FooterComponent } from './footer/footer.component';
import { ExplanationComponent } from './explanation/explanation.component';
import { Explanation1Component } from './explanation1/explanation1.component';
import { Explanation2Component } from './explanation2/explanation2.component';
import { NavbarComponent } from './navbar/navbar.component';
import { EmployeeCreateComponent } from './employee-create/employee-create.component';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { GameviwerComponent } from './gameviwer/gameviwer.component';

import { ContactComponent } from './contact/contact.component';
import { ServiceComponent } from './service/service.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { BlogComponent } from './compo/blog/blog.component';
import { PortfolioComponent } from './compo/portfolio/portfolio.component';
import { FeedbackComponent } from './compo/feedback/feedback.component';
import { BaseserviceService } from './base/baseservice.service';
import { VerificationComponent } from './verification/verification.component';
import { CommonService } from './common.service';




@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    RegisterComponent,
    QuestionComponent,
    FooterComponent,
    ExplanationComponent,
    Explanation1Component,
    Explanation2Component,
    NavbarComponent,
    EmployeeCreateComponent,
    EmployeeEditComponent,
    EmployeeListComponent,
    GameviwerComponent,
    
    ContactComponent,
    ServiceComponent,
    SignUpComponent,
    BlogComponent,
    PortfolioComponent,
    FeedbackComponent,
    VerificationComponent,
    ],
  imports: [BrowserAnimationsModule,FormsModule,CountdownTimerModule,
    BrowserModule,HttpClientModule,ReactiveFormsModule,MatSnackBarModule,
    AppRoutingModule,MatCardModule,MatToolbarModule,MatIconModule,MatButtonModule,MatMenuModule,MatFormFieldModule,MatInputModule,MatSlideToggleModule
  ],
  providers: [BaseserviceService,CommonService]
  
  ,bootstrap: [AppComponent]
})
export class AppModule { }
