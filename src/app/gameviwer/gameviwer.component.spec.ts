import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameviwerComponent } from './gameviwer.component';

describe('GameviwerComponent', () => {
  let component: GameviwerComponent;
  let fixture: ComponentFixture<GameviwerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameviwerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameviwerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
