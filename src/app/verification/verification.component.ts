import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,ActivatedRoute} from '@angular/router';
import { CommonService } from '../common.service';
@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css']
})
export class VerificationComponent implements OnInit {

  VerificationForm: FormGroup;
  returnUrl: any;
  
  emailotp:string;
  mobileotp:string;
  availabedrugs: any;
  item:any;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    
    private  activated:ActivatedRoute,
    private commonservce:CommonService) 
    { }
  get request(){
    return this.VerificationForm.controls
  }
  ngOnInit() {
    this.activated.queryParams.subscribe((params)=>{
      console.log(params);
    })
    this.VerificationForm = this.formBuilder.group({
      emailotp: ['', [Validators.required]],
      mobileotp: ['', [Validators.required]]
    
    })
    
    this.commonservce.getDrugs().subscribe(drugs=>{
      this.item=drugs;
      console.log(drugs);
    })
  
  }
  Submit(){
  }
  
  getemailotpError(){
    return this.request.emailotp.hasError('required')?'This field is required':'';
  }
  getmobileotpError(){
    return this.request.mobileotp.hasError('required')?'This field is required':'';
  }

}
