import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Explanation1Component } from './explanation1.component';

describe('Explanation1Component', () => {
  let component: Explanation1Component;
  let fixture: ComponentFixture<Explanation1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Explanation1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Explanation1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
