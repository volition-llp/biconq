import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../environments/environment';

import { BaseserviceService } from './base/baseservice.service';

export interface temp{
  name:string;
  email:string;
  phone:number;
}
@Injectable({
  providedIn: 'root'
})

export class CommonService {
  
   
// message:string;
// setmessage(data){
// this.message=data;
// }
// getmessage(){
// return this.message;

// }
private availabledrugs=new BehaviorSubject<temp[]>(null)
  constructor(private http:HttpClient,
    private baseService: BaseserviceService) { }
    loadDrugs(drugs:temp[])
    {
      this.availabledrugs.next(drugs)
    }
    getDrugs():Observable<temp[]>{
      return this.availabledrugs.asObservable();
    }

    
    registration(data: any) {
    
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'responseType': 'json'
    });
      return this.http.post(`${environment.apiUrl}signup`,data,{headers:headers}).pipe(map((res:any)=>{
        this.baseService.snack(res.message);
        return res;
      }),
      catchError(this.baseService.handleError('registration()',[]))
      )
  }
}